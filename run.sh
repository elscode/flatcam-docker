#!/bin/bash

docker run --rm     -p 5900:5900 --privileged -e DISPLAY=host.docker.internal:0  --add-host=host.docker.internal:host-gateway  -v /Users/juliencolant/FlatCAM_beta_8.994_sources/:/opt/flatcam/ -v /Users/juliencolant/KICAD-WORKSPACE/:/opt/KICAD_WORKSPACE   -it flatcam-8994:latest python3 /opt/flatcam/Flatcam.py


