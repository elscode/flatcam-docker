FROM ubuntu:20.04
#FROM python:3.11

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update
ENV LIBGL_ALWAYS_SOFTWARE=1



RUN apt update &&  apt install -y xvfb  libgl1-mesa-dev sudo mc python3-reportlab python3-vispy python3-opengl python3-matplotlib python3-rasterio libgdal-dev libgl1-mesa-dri python3-pip python3-pyqt5 python3-pyqt5.qtopengl libpng-dev libfreetype6 libfreetype6-dev python3-dev python3-simplejson python3-numpy python3-scipy libgeos-dev python3-shapely python3-rtree python3-tk libspatialindex-dev python3-gdal python3-lxml 

RUN apt-get install -y xfce4 xfce4-goodies tightvncserver x11vnc xterm net-tools
    
RUN pip install --upgrade pip
RUN sudo pip3 install -U pip setuptools
RUN sudo -H python3 -m pip install --upgrade pip  numpy==1.23.1 shapely==1.7.1 rtree tk lxml cycler vispy==0.9.0 python-dateutil>=2.1 six kiwisolver dill pyopengl svg.path ortools freetype-py fontTools ezdxf==0.9.0 matplotlib==3.1 cycler>=0.10

RUN sudo -H python3 -m pip install --upgrade    qrcode  reportlab svglib pyserial

RUN useradd -rm -d /home/julien -s /bin/bash -g root -G sudo -u 1001 julien
RUN echo 'julien:azerty' | chpasswd
RUN groupadd julien
RUN echo "julien ALL=(ALL:ALL) ALL" >> /etc/sudoers
RUN rm /bin/sh & /bin/bash -c "ln -s /bin/bash /bin/sh"

RUN mkdir /root/.vnc \
    && echo "azerty" | vncpasswd -f > /root/.vnc/passwd \
    && chmod 600 /root/.vnc/passwd

USER root
ENV USER=root
RUN echo "#!/bin/bash" > /root/.vnc/xstartup
RUN echo "/usr/bin/xrdb $HOME/.Xresources" >> /root/.vnc/xstartup
RUN echo "startxfce4 &" >> /root/.vnc/xstartup
RUN chmod +x /root/.vnc/xstartup

RUN echo "#!/bin/bash" > /root/start_vnc.sh
RUN echo "tightvncserver :0 -geometry 1600x1200 -depth 24 && tail -f /dev/null" >> /root/start_vnc.sh
RUN chmod +x /root/start_vnc.sh




CMD ["/root/start_vnc.sh"]
